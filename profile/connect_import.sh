#!/bin/bash
# Log in to Profile Maestro Proxy From Connect
#
#
# Connect import
#
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
cd $DIR

echo "*******************"
echo "Import Script start"
echo "*******************"
#echo "connect_import.sh $DIR"

BASE='https://beta.livecare.ca'
KEY='a6f0dbdf-40b8-4781-9be4-61805c09cf7c'
CURRENT=$(date +"%F-%H-%M-%S")
echo " $CURRENT"
#echo " "

if /bin/bash messagesin.sh; then
    DATA=$(cat newmessage | python -m json.tool | grep data | awk '{print($2)}' | sed -e 's/^"//' -e 's/,$//' | sed -e 's/"$//')
    DATA=$(echo $DATA | base64 --decode)

    curl -s -d "$DATA" \
            -H 'Content-Type: application/json' \
            -H "Organization-Key: $KEY" \
            -X POST $BASE/appointment > import/import-$CURRENT.html
#    CODE=$(cat import/import-$CURRENT.html | python -c "import sys, json; sys.stdout.write(json.load(sys.stdin)['appointmentUUID'])")
#    mv import/import-$CURRENT.html import/import-$CURRENT-$CODE.html
#    exit 0
else
    echo "*******************"
    echo "Import Script end  "
    echo "*******************"
    echo ""
    exit 1
fi

echo "*******************"
echo "Import Script end  "
echo "*******************"
echo ""
exit 0
