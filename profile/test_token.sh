#!/bin/bash

TOKEN_EXPIRE=$(cat token | python -c "import sys, json; sys.stdout.write(json.load(sys.stdin)['.expires'])")
TOKEN_EXPIRE_YEAR=$(echo $TOKEN_EXPIRE | cut -c1-4)
TOKEN_EXPIRE_MONTH=$(echo $TOKEN_EXPIRE | cut -c6-7)
TOKEN_EXPIRE_DAY=$(echo $TOKEN_EXPIRE | cut -c9-10)
TOKEN_EXPIRE_HOUR=$(echo $TOKEN_EXPIRE | cut -c12-13)
TOKEN_EXPIRE_MINUTE=$(echo $TOKEN_EXPIRE | cut -c15-16)
TOKEN_EXPIRE_SECOND=$(echo $TOKEN_EXPIRE | cut -c18-19)
TOKEN_EXPIRE_DATE=$TOKEN_EXPIRE_YEAR$TOKEN_EXPIRE_MONTH$TOKEN_EXPIRE_DAY$TOKEN_EXPIRE_HOUR$TOKEN_EXPIRE_MINUTE$TOKEN_EXPIRE_SECOND

DATE=$(date --utc +"%Y%m%d%H%M%S")

echo "Token Expires $TOKEN_EXPIRE_DATE"
echo "Current time  $DATE"

# Test if we need to update the token
# Greater than or equal
if [ $DATE -gt $TOKEN_EXPIRE_DATE ]; then
    echo " token expired!"
    exit 1
fi
echo " token Valid"
exit 0
