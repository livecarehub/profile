#!/bin/bash
# Log in to Profile Maestro Proxy From Connect
#
# Connect delete
#
# call this script by ./connect_delte <9_digit_code>
#
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
cd $DIR

CODE=$1
BASE='https://beta.livecare.ca'
KEY='a6f0dbdf-40b8-4781-9be4-61805c09cf7c'
CURRENT=$(date +"%F-%H-%M-%S")

re='^[0-9]+$'
if ! [[ $CODE =~ $re ]] ; then
   echo " ./connect_delte <9_digit_code>"
   echo "error: Not a number" >&2
   exit 1
fi

echo " removing appointment $CODE..."
curl -s -H 'Content-Type: application/json' \
        -H "Organization-Key: $KEY" \
        -X DELETE $BASE/appointment?appointmentUUID=$CODE > deleted/delete-$CURRENT-$CODE.html

echo " done"
exit 0
