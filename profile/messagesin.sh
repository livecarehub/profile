#!/bin/bash
# Log in to Profile Maestro Proxy From Connect
#
#
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
#DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
#cd $DIR
DIR="$(pwd)"
#echo "messagein.sh $DIR"

# check if we have a token
if [ -e token ]; then
  echo " Missing login token..."
  /bin/bash login.sh
fi

TOKEN_EXPIRE=$(cat token | python -c "import sys, json; sys.stdout.write(json.load(sys.stdin)['.expires'])")
TOKEN_EXPIRE_YEAR=$(echo $TOKEN_EXPIRE | cut -c1-4)
TOKEN_EXPIRE_MONTH=$(echo $TOKEN_EXPIRE | cut -c6-7)
TOKEN_EXPIRE_DAY=$(echo $TOKEN_EXPIRE | cut -c9-10)
TOKEN_EXPIRE_HOUR=$(echo $TOKEN_EXPIRE | cut -c12-13)
TOKEN_EXPIRE_MINUTE=$(echo $TOKEN_EXPIRE | cut -c15-16)
TOKEN_EXPIRE_SECOND=$(echo $TOKEN_EXPIRE | cut -c18-19)
TOKEN_EXPIRE_DATE=$TOKEN_EXPIRE_YEAR$TOKEN_EXPIRE_MONTH$TOKEN_EXPIRE_DAY$TOKEN_EXPIRE_HOUR$TOKEN_EXPIRE_MINUTE$TOKEN_EXPIRE_SECOND

ADATE=$(date --utc +"%Y%m%d%H%M%S")
# add 10 seconds, for time take for complete script to complete. This is a fix for duplicate apt
DATE=$(date -s "10 seconds" --utc +"%Y%m%d%H%M%S")

echo "Actual time   $ADATE"
echo "Token Expires $TOKEN_EXPIRE_DATE"
echo "Test time     $DATE"

# Test if we need to update the token
# Greater than or equal

if [ $DATE -gt $TOKEN_EXPIRE_DATE ]; then
    echo " token expired!"
    /bin/bash login.sh
fi



# get security token from login
TOKEN=$(cat token | python -c "import sys, json; sys.stdout.write(json.load(sys.stdin)['access_token'])")

#echo $TOKEN
echo " Making call for new message..."

curl -s -X GET \
     -H 'Accept: text/html' \
     -H "Authorization: Bearer $TOKEN" \
     'https://881.myaccession.com/Maestro/maestro/messagesin' > newmessage

# Check if message empty
MSG=$(cat newmessage | sed -e 's/^{//' -e 's/}$//')
if [ -z "$MSG" ]; then
    echo " No new messages"
    exit 1
fi

# get from last call handle/message
HANDLE=$(cat newmessage | python -c "import sys, json; sys.stdout.write(json.load(sys.stdin)['handle'])")
echo "Handle id " $HANDLE
DATA='{
    "handle": '
DATA2='}'

echo " Closing the message..."
# close the messsage/ marking it as received
curl -s -d "$DATA$HANDLE$DATA2" \
     -H 'Accept: application/json' \
     -H 'transfer-encoding: chunked' \
     -H "Authorization: Bearer $TOKEN" \
     -X POST 'https://881.myaccession.com/Maestro/maestro/messagesin/close' > closemessage

echo " Done..."
DATA=$(cat newmessage | python -m json.tool | grep data | awk '{print($2)}' | sed -e 's/^"//' -e 's/,$//' | sed -e 's/"$//')
echo " Message:"
echo $DATA | base64 --decode
echo " "

exit 0
