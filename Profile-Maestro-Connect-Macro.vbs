' In the P rofile c ontext:
'   StoredObject is the original starting object as in original appointment when an existing one is being loaded
'   ChangedObject is the new object values as changed from the original
' when a new object item such as a new appointment is created, the StoredObject is blank and the ChangedObject has all of the appointment parameters

Sub Main()
' Following "if" restricts macro to specified test user name 
'if ChangedObject.PatientId = 3 then
' If this is a virual session appointment
    sTypeCode = ChangedObject.TypeCode
    if Left(sTypeCode,7) = "VIRTUAL" then
' Setup link to Maestro Outgoing Queue Manager
        Dim timeOffset
        set qp = Profile.MakeQueueParams
        qp.QueueName = "Livecare Telemedicine Maestro Outgoing"
        set qs = Profile.LoadQueues(qp)
        set q = qs.Item(0)
        set strJSONToSend = q.CreateQueueItem
' Get the patient and provider details for the current appointment
        set pat = Profile.LoadPatient(ChangedObject.PatientID)
        set pro = Profile.LoadProvider(ChangedObject.ProviderCode)
        sApptStatus = ChangedObject.DisplayStatusAsString
        sFullName = ""
        Set oApnt = CreateAppointment
            with oApnt
               .PatientID = ChangedObject.PatientID
               .BookTime = ChangedObject.BookTime
               .Duration = ChangedObject.Duration
               .ConfirmTime = ChangedObject.Confirmtime
               .Comment = ChangedObject.Comment
        End With
        sFullName = pat.FirstName & " " & pat.LastName
        sPatientEmail = trim(pat.Email)
'        if len(sPatientEmail) = 0 then
'          sPatientEmail = "kdulay@coastmedical.ca"
'          oApnt.Comment = "NO PATIENT EMAIL " & trim(oApnt.Comment)
'        End If
        sProviderEmail = pro.Email
        sProviderFullName = pro.FullName
        sProviderLastName = pro.LastName
' Setup the time zone and number of hours offset from GMT
        dstStart = FindDSTStartDate(DatePart("yyyy",oApnt.BookTime))
        dstEnd = FindDSTEndDate(DatePart("yyyy",oApnt.BookTime))
        if (oApnt.BookTime >= dstStart AND oApnt.BookTime < dstEnd) Then
            timeOffset = 7
            sOffset = "+700"
          Else          
            timeOffset = 8
            sOffset = "+800"
        End If
        sTimeZone = "America/Vancouver"
        dBookTime = (GetDateMill(oApnt.BookTime) + (timeOffset*3600))*1000
        if sApptStatus = "Cancelled" then
            sAction = "delete"
          else 
            sAction = "create"
        End If
' Construct the JSON text formatted message to go to Maestro
'        strJSONToSend.Blob = "{ " & _
       strJSONToSend.Blob = "5803d241-acd5-40b3-a563-81898baef076" & Chr(13) & Chr(10) & "{ " & _
            " ""providerEmail"": " & """" & sProviderEmail & """" & "," & _
            " ""appointment"": " & "{" & _
              " ""date"": " & dBookTime & "," & _
              " ""offset"": " & """" & sOffset & """" & "," & _
              " ""timeZoneName"": " & """" & sTimeZone & """" & "," & _
              " ""patientEmail"": " & """" & sPatientEmail & """" & "," & _
              " ""patientName"": " & """" & sFullName & """" & "," & _
              " ""isPatientEmail"": " & "true" & "," & _
              " ""additionalInfo"": " & """" & oApnt.Comment & """" & "," & _
              " ""action"": " & """" & sAction & """" & _
              " }" & _
            "  }"
    end if
' End of test user if
'end if
End Sub

Function GetDateMill(ByVal aDate)
  If aDate <> 0 then
  fromDate = CDate("1970/01/01 00:00:00")
  GetDateMill = DateDiff("s",fromDate,aDate)
  Else
    GetDateMill = 0
  End If
End Function

Function FindDSTStartDate(aYear)
' DST starts at 0300 on 2nd Sunday in march
  varDate = CDate(aYear & "/3/1 03:00:00")
        sundayCnt = 0 
  Do Until (sundayCnt >= 2)
    varDate = DateAdd("d",1,varDate)
          If (DatePart("w",varDate) = 1) then
              sundayCnt = sundayCnt + 1
          End If          
  Loop
  FindDSTStartDate = varDate
End Function

Function FindDSTEndDate(aYear)
' DST Ends at 0200 on the first sunday of November
  varDate = CDate(aYear & "/11/1 02:00:00")
        sundayCnt = 0 
  Do Until (sundayCnt >= 1)
    varDate = DateAdd("d",1,varDate)
          If (DatePart("w",varDate) = 1) then
              sundayCnt = sundayCnt + 1
          End If          
  Loop
  FindDSTEndDate = varDate
End Function