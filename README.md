# Profile

This repository contains macros, scripts and other any other programming elements external to Connect for enabling Profile - Connect data exchange.

The repository was originally created to contain the macros used to send Profile appointment data to Connect via the Intrahealth Profile Maestro messaging service.  Initially there are 2 macros "" for send over newly created appointments and "" to inform Connect that an appointment has been deleted.